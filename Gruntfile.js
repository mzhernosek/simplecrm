'use strict';
module.exports = function (grunt) {

    grunt.initConfig({
        watch: {
            files: [
                "front/admin-lte/build/less/*.less",
                "front/admin-lte/build/less/skins/*.less"
            ],
            tasks: ["less"]
        },
        less: {
            development: {
                options: {
                    compress: true
                },
                files: {
                    "web/css/style.min.css": "front/admin-lte/build/less/AdminLTE.less"
                }
            }
        },
        tt: {
            development: {
                options: {
                    compress: true
                },
                files: {
                    "web/css/style.min.css": "bower_components/admin-lte/build/less/AdminLTE.less"
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks('grunt-cssjanus');

    grunt.registerTask('default', ['watch']);
};