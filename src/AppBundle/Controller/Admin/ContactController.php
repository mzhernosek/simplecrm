<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\BaseController as Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\ContactType;
use AppBundle\Entity\Contact;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * @Route("/admin/contact")
 */
class ContactController extends Controller
{
    /**
     * @Route("s", name="admin_contacts")
     */
    public function indexAction(Request $request)
    {
        $contacts = $this->getDoctrine()->getRepository('AppBundle:Contact')->findAll();


        return $this->renderForAjax('admin/contact/index.html.twig', array(
            'contacts' => $contacts
        ));
    }

    /**
     * @Route("/{id}/show", name="admin_contact_show")
     */
    public function showAction(Request $request, $id)
    {
        $contact = $this->getDoctrine()->getRepository('AppBundle:Contact')->find($id);

        return $this->renderForAjax('admin/contact/show.html.twig', array(
            'contact' => $contact,
        ));
    }

    /**
     * @Route("/{id}/edit", name="admin_contact_edit")
     */
    public function editAction(Request $request, $id)
    {
        $contact = $this->getDoctrine()->getRepository('AppBundle:Contact')->find($id);
        $form = $this->createForm(new ContactType(), $contact);

        if ($request->isMethod('post')) {
            $data = $request->request->all();
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush();

                $response = new JsonResponse(['status' => true]);
                return $response;
            }
        }


        if($request->isXmlHttpRequest()) {
            return $this->render('admin/contact/edit.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/add", name="admin_contact_add")
     */
    public function addAction(Request $request)
    {
        $contact = new Contact();

        $form = $this->createForm(new ContactType(), $contact);

        if ($request->isMethod('post')) {
            $data = $request->request->all();
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush();

                $response = new JsonResponse(['status' => true]);
                return $response;
            }
        }

        if($request->isXmlHttpRequest()) {
            return $this->render('admin/contact/add.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/remove", name="admin_contact_remove")
     */
    public function removeAction(Request $request) {
        $status = false;

        if ($request->isMethod('post')) {
            $id = $request->request->get('id');
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            $contact = $doctrine->getRepository('AppBundle:Contact')->find($id);
            $em->remove($contact);
            $em->flush();

            $status = true;
        }

        return new JsonResponse(['status' => $status]);
    }
}
