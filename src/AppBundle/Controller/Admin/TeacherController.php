<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/teachers")
 */
class TeacherController extends Controller
{
    /**
     * @Route("", name="admin_teachers")
     */
    public function indexAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            return $this->render('admin/teacher/index.html.twig');
        }

        return $this->render('layout.html.twig');
    }
}
