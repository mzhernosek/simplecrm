<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        if($request->isXmlHttpRequest()) {
            return $this->render('admin/user/index.html.twig', array('users' => $users));
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/user/{id}/show", name="user_show")
     */
    public function showAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        $projects = [];
        $tasks = [];

        if($request->isXmlHttpRequest()) {
            return $this->render('admin/user/show.html.twig', array(
                'user' => $user,
                'projects' => $projects,
                'tasks' => $tasks
            ));
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/admin/user/add", name="admin_user_add")
     */
    public function addAction(Request $request)
    {
        $user = new User();
        $user->addRole('ROLE_USER');

        $form = $this->createForm(new UserType(), $user);
        if ($request->isMethod('post')) {
            $data = $request->request->all();
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {

                $userManager = $this->get('fos_user.user_manager');

                $user = $userManager->createUser();
                $user->setEmail($data['email']);
                $user->setUsername($data['username']);
                $user->setUsernameCanonical($data['username']);
                $user->setPlainPassword($data['password']);
                $user->setEnabled(true);
                $user->addRole($data['role']);

                if ($data['contact']) {
                    $contact = $this->getDoctrine()->getRepository('AppBundle:Contact')->find($data['contact']);
                    $user->setContact($contact);
                }


                $userManager->updateUser($user);

                $response = new JsonResponse(array('status'=>true));
                return $response;
            }
        }



        if($request->isXmlHttpRequest()) {
            return $this->render('admin/user/add.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/admin/user/{id}/edit", name="admin_user_edit")
     */
    public function editAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        $form = $this->createForm(new UserType(), $user);
        $form->remove('password');

        if ($request->isMethod('post')) {
            $data = $request->request->all();
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $response = new JsonResponse(['status' => true]);
                return $response;
            }
        }


        if($request->isXmlHttpRequest()) {
            return $this->render('admin/user/edit.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/admin/user/remove", name="admin_user_remove")
     */
    public function removeAction(Request $request) {
        $status = false;

        if ($request->isMethod('post')) {
            $data = $request->request->all();
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserBy(['id' => $data['id']]);
            $userManager->deleteUser($user);

            $status = true;
        }

        return new JsonResponse(['status' => $status]);
    }
}
