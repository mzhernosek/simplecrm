<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    public function renderForAjax($template, $data=[])
    {
        return $this->get('request')->isXmlHttpRequest()
            ? $this->render($template, $data)
            : $this->render('layout.html.twig');
    }
}
