<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\BaseController as Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\ProjectType;
use AppBundle\Entity\Project;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProjectController extends Controller
{
    /**
     * @Route("/projects", name="projects")
     */
    public function indexAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $projects = $this->get('project_manager')->getProjects($this->getUser());

            return $this->render('project/index.html.twig', ['projects' => $projects]);
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/admin/project/add", name="admin_project_add")
     */
    public function addAction(Request $request)
    {
        $project = new Project();

        $form = $this->createForm(new ProjectType(), $project);

        if ($request->isMethod('post')) {
            $data = $request->request->all();
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($project);
                $em->flush();

                return new JsonResponse(['status' => true]);
            }
        }


        return $this->renderForAjax('project/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/project/{id}/edit", name="admin_project_edit")
     */
    public function editAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $project = $this->getDoctrine()->getRepository('AppBundle:Project')->find($id);
            $form = $this->createForm(new ProjectType(), $project);

            if ($request->isMethod('post')) {
                $data = $request->request->all();
                $form->submit($data);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($project);
                    $em->flush();

                    $response = new JsonResponse(['status' => true]);
                    return $response;
                }
            }

            return $this->render('project/edit.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('layout.html.twig');
    }

    /**
     * @Route("/admin/project/remove", name="admin_project_remove")
     */
    public function removeAction(Request $request) {
        $status = false;

        if ($request->isMethod('post')) {
            $id = $request->request->get('id');
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            $project = $doctrine->getRepository('AppBundle:Project')->find($id);
            $em->remove($project);
            $em->flush();

            $status = true;
        }

        return new JsonResponse(['status' => $status]);
    }

    /**
     * @Route("/project/{id}/show", name="project_show")
     */
    public function showAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $project = $this->getDoctrine()->getRepository('AppBundle:Project')->find($id);

            return $this->render('project/show.html.twig', array(
                'project' => $project
            ));
        }

        return $this->render('layout.html.twig');
    }
}
