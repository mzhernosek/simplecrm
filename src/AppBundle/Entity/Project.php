<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBUndle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="project")
 */
class Project
{
    const STATUS_IN_TREATY = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_CLOSED = 2;
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $title;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects_lead")
     * @ORM\JoinColumn(name="lead_id", referencedColumnName="id")
     */
    protected $lead;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects_client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;


    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="text")
     */
    protected $access_info;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    protected $status = 0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }


    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set access_info
     *
     * @param string $accessInfo
     * @return Project
     */
    public function setAccessInfo($accessInfo)
    {
        $this->access_info = $accessInfo;

        return $this;
    }

    /**
     * Get access_info
     *
     * @return string 
     */
    public function getAccessInfo()
    {
        return $this->access_info;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Project
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set lead
     *
     * @param \AppBundle\Entity\User $lead
     * @return Project
     */
    public function setLead(\AppBundle\Entity\User $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return \AppBundle\Entity\User 
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\User $client
     * @return Project
     */
    public function setClient(\AppBundle\Entity\User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\User 
     */
    public function getClient()
    {
        return $this->client;
    }
}
