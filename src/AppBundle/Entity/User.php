<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Contact")
     */
    protected $contact;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="lead")
     */
    protected $projects_lead;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="client")
     */
    protected $projects_client;



    public function __construct()
    {
        parent::__construct();

    }

    public function setUsernameCanonical($username)
    {
        $this->usernameCanonical = $this->email;

        return $this;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param \AppBundle\Entity\Contact $contact
     * @return User
     */
    public function setContact(\AppBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \AppBundle\Entity\Contact 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set projects_lead
     *
     * @param \AppBundle\Entity\Project $projectsLead
     * @return User
     */
    public function setProjectsLead(\AppBundle\Entity\Project $projectsLead = null)
    {
        $this->projects_lead = $projectsLead;

        return $this;
    }

    /**
     * Get projects_lead
     *
     * @return \AppBundle\Entity\Project 
     */
    public function getProjectsLead()
    {
        return $this->projects_lead;
    }

    /**
     * Set projects_client
     *
     * @param \AppBundle\Entity\Project $projectsClient
     * @return User
     */
    public function setProjectsClient(\AppBundle\Entity\Project $projectsClient = null)
    {
        $this->projects_client = $projectsClient;

        return $this;
    }

    /**
     * Get projects_client
     *
     * @return \AppBundle\Entity\Project 
     */
    public function getProjectsClient()
    {
        return $this->projects_client;
    }

    /**
     * Add projects_lead
     *
     * @param \AppBundle\Entity\Project $projectsLead
     * @return User
     */
    public function addProjectsLead(\AppBundle\Entity\Project $projectsLead)
    {
        $this->projects_lead[] = $projectsLead;

        return $this;
    }

    /**
     * Remove projects_lead
     *
     * @param \AppBundle\Entity\Project $projectsLead
     */
    public function removeProjectsLead(\AppBundle\Entity\Project $projectsLead)
    {
        $this->projects_lead->removeElement($projectsLead);
    }

    /**
     * Add projects_client
     *
     * @param \AppBundle\Entity\Project $projectsClient
     * @return User
     */
    public function addProjectsClient(\AppBundle\Entity\Project $projectsClient)
    {
        $this->projects_client[] = $projectsClient;

        return $this;
    }

    /**
     * Remove projects_client
     *
     * @param \AppBundle\Entity\Project $projectsClient
     */
    public function removeProjectsClient(\AppBundle\Entity\Project $projectsClient)
    {
        $this->projects_client->removeElement($projectsClient);
    }
}
