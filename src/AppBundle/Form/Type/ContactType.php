<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Имя | Название',
                'attr' => array(
                    'ng-model' => 'contact.name',
                    'ng-initial' => ''
                )
            ))
            ->add('phones', 'text', array(
                'label' => 'Телефоны',
                'attr' => array(
                    'ng-model' => 'contact.phones',
                    'ng-initial' => ''
                )
            ))
            ->add('email', 'email', array(
                'label' => 'Email',
                'attr' => array(
                    'ng-model' => 'contact.email',
                    'ng-initial' => ''
                )
            ))
            ->add('description', 'textarea', array(
                'label' => 'Описание',
                'attr' => array(
                    'ng-model' => 'contact.description',
                    'ng-initial' => ''
                )
            ))
            

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'contact';
    }
}