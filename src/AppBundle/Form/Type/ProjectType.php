<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Entity\Project;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Название',
                'attr' => array(
                    'ng-model' => 'project.title',
                    'ng-initial' => ''
                )
            ))
            ->add('description', 'textarea', array(
                'label' => 'Описание',
                'attr' => array(
                    'ng-model' => 'project.description',
                    'ng-initial' => ''
                )
            ))
            ->add('access_info', 'textarea', array(
                'label' => 'Информация доступов',
                'attr' => array(
                    'ng-model' => 'project.access_info',
                    'ng-initial' => ''
                ),
                'required' => false
            ))
            ->add('status', 'choice', array(
                'label' => 'Статус',
                'choices' => array(
                    Project::STATUS_IN_TREATY => 'Ведутся переговоры',
                    Project::STATUS_IN_PROGRESS => 'Ведется разработка',
                    Project::STATUS_CLOSED => 'Закрыт'
                ),
                'attr' => array(
                    'ng-model' => 'project.status',
                    'ng-initial' => ''
                )
            ))
            ->add('lead', 'entity', array(
                'label' => 'Руководитель',
                'class' => 'AppBundle:User',
                'property' => 'username',
                'required' => false,
                'attr' => array(
                    'ng-model' => 'project.lead',
                    'ng-initial' => ''
                )
            ))
            ->add('client', 'entity', array(
                'label' => 'Клиент',
                'class' => 'AppBundle:User',
                'property' => 'username',
                'required' => false,
                'attr' => array(
                    'ng-model' => 'project.client',
                    'ng-initial' => ''
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'project';
    }
}