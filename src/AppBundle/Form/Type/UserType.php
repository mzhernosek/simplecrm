<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'label' => 'Имя пользователя',
                'attr' => array(
                    'ng-model' => 'user.username',
                    'ng-initial' => ''
                )
            ))
            ->add('email', 'email', array(
                'attr' => array(
                    'ng-model' => 'user.email',
                    'ng-initial' => ''
                )
            ))
            ->add('password', 'text', array(
                'label' => 'Укажите пароль',
                'attr' => array(
                    'ng-model' => 'user.password'
                )
            ))
            ->add('role', 'choice', array(
                'label' => 'Роль',
                'attr' => array(
                    'ng-model' => 'user.role',
                    'ng-initial' => ''
                ),
                'choices' => array(
                    'ROLE_USER' => 'Пользователь',
                    'ROLE_ADMIN' => 'Клиент',
                    'ROLE_SUPER_ADMIN' => 'Владелец'
                ),
                'mapped' => false
            ))
            ->add('contact', 'entity', array(
                'class' => 'AppBundle:Contact',
                'property' => 'name',
                'required' => false,
                'label' => 'Контакт',
                'attr' => array(
                    'ng-model' => 'user.contact',
                    'ng-initial' => ''
                )


            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'user';
    }
}