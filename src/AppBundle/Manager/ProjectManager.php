<?php

namespace AppBundle\Manager;

use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\User;

class ProjectManager
{
    private $container;
    private $em;
    private $repository;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->repository = $this->container->get('doctrine')->getRepository('AppBundle:Project');
    }

    public function getProjects(User $user)
    {
        $projects = [];

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            $projects = $this->repository->findAll();
        } elseif ($user->hasRole('ROLE_ADMIN')) {
            $projects = $this->repository->findBy(['client' => $user]);
        }

        return $projects;
    }

}