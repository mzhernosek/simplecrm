var App = angular.module('app', ['ui.bootstrap', 'ngRoute'])
    .config(function($interpolateProvider, $locationProvider, $routeProvider, $httpProvider){
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');

        var base = window.base;

        $routeProvider
            .when('/', {
                templateUrl: base,
                title: 'Главная',
                controller : "MainController"
            })
            .when('/projects', {
                templateUrl: base+'projects',
                title: 'Проекты',
                controller : "ProjectController"
            })
            .when('/admin/users', {
                templateUrl: base+'admin/users',
                title: 'Пользователи'
            })
            .when('/user/:id/show', {
                templateUrl: function(urlattr){
                    return base+'user/' + urlattr.id + '/show';
                },
                title: 'Управление пользователем'
            })
            .when('/admin/user/:id/edit', {
                templateUrl: function(urlattr){
                    return base+'admin/user/' + urlattr.id + '/edit';
                },
                title: 'Редактирование пользователя',
                controller : "UserController"
            })
            .when('/admin/user/add', {
                templateUrl: base+'admin/user/add',
                title: 'Добавление пользователя'
            })
            .when('/admin/contacts', {
                templateUrl: base+'admin/contacts',
                title: 'Контакты'
            })
            .when('/admin/contact/add', {
                templateUrl: base+'admin/contact/add',
                title: 'Добавление контакта'
            })
            .when('/admin/contact/:id/show', {
                templateUrl: function(urlattr){
                    return base+'admin/contact/' + urlattr.id + '/show';
                },
                title: 'Просмотр контакта'
            })
            .when('/admin/contact/:id/edit', {
                templateUrl: function(urlattr){
                    return base+'admin/contact/' + urlattr.id + '/edit';
                },
                title: 'Редактирование контакта',
                controller : "ContactController"
            })
            .when('/admin/project/add', {
                templateUrl: function(urlattr){
                    return base+'admin/project/add';
                },
                title: 'Добавление проекта',
                controller: 'ProjectController'
            })
            .when('/admin/project/:id/edit', {
                templateUrl: function(urlattr){
                    return base+'admin/project/' + urlattr.id + '/edit';
                },
                title: 'Редактирование проекта',
                controller : "ProjectController"
            })
            .when('/project/:id/show', {
                templateUrl: function(urlattr){
                    return base+'project/' + urlattr.id + '/show';
                },
                title: 'Просмотр проекта'
            })
        ;


        $locationProvider.html5Mode(true);


    })
    .run(function($rootScope, $templateCache, $window) {
        $rootScope.$on('$viewContentLoaded', function() {

            $templateCache.removeAll();
        });

        $rootScope.$on('$locationChangeStart', function(event, next, current){

        });

        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {

            if (!current) {
                $window.location.reload();
                return;
            }



            $rootScope.pageTitle = current.title;
        });

    });