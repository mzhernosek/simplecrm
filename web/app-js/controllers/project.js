App
    .controller("ProjectController", ["$scope", "$http", "$location", "$window", "$route", function ($scope, $http, $location, $window, $route) {

        $scope.tabs = [
            { title:'Dynamic Title 1', content:'Dynamic content 1' },
            { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
        ];

        $scope.remove = function(id) {
            if ($window.confirm('Вы действительно хотите удалить проект с id='+id+' ?')) {
                $http.post(
                    window.base+'admin/project/remove',
                    serializeData({id:id}),
                    { headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }}
                ).success(function(data) {
                        $location.path('projects');
                        $route.reload();
                    });
            }
        };

        $scope.submit = function(project) {

            $http.post(
                window.base.slice(0,-1)+$location.url(),
                serializeData(project),
                { headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }}
            ).success(function(data) {
                    $location.path('projects');
                });


        };


        function serializeData( data ) {

            // If this is not an object, defer to native stringification.
            if ( ! angular.isObject( data ) ) {

                return( ( data == null ) ? "" : data.toString() );

            }

            var buffer = [];

            // Serialize each key in the object.
            for ( var name in data ) {

                if ( ! data.hasOwnProperty( name ) ) {

                    continue;

                }

                var value = data[ name ];

                buffer.push(
                    encodeURIComponent( name ) +
                    "=" +
                    encodeURIComponent( ( value == null ) ? "" : value )
                );

            }

            // Serialize the buffer and clean it up for transportation.
            var source = buffer
                    .join( "&" )
                    .replace( /%20/g, "+" )
                ;

            return( source );

        }
    }]);
