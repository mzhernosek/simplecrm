App
    .controller("UserController", ["$scope", "$http", "$location", "$window", "$route", function ($scope, $http, $location, $window, $route) {

        $scope.remove = function(userId) {
            if ($window.confirm('Вы действительно хотите удалить пользователя с id='+userId+' ?')) {
                $http.post(
                    window.base+'admin/user/remove',
                    serializeData({id:userId}),
                    { headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }}
                ).success(function(data) {
                    console.log(data);
                    $location.path('admin/users');
                    $route.reload();
                });
            }
        };

        $scope.submit = function(user) {
            $http.post(
                window.base.slice(0,-1)+$location.url(),
                serializeData(user),
                { headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }}
            ).success(function(data) {
                    console.log(data);
                    $location.path('admin/users');
            });
        };

        function serializeData( data ) {

            // If this is not an object, defer to native stringification.
            if ( ! angular.isObject( data ) ) {

                return( ( data == null ) ? "" : data.toString() );

            }

            var buffer = [];

            // Serialize each key in the object.
            for ( var name in data ) {

                if ( ! data.hasOwnProperty( name ) ) {

                    continue;

                }

                var value = data[ name ];

                buffer.push(
                    encodeURIComponent( name ) +
                    "=" +
                    encodeURIComponent( ( value == null ) ? "" : value )
                );

            }

            // Serialize the buffer and clean it up for transportation.
            var source = buffer
                    .join( "&" )
                    .replace( /%20/g, "+" )
                ;

            return( source );

        }
    }]);
