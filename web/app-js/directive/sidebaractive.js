App
    .directive('activeLink', ['$location', function(location) {
        return {
            link: function(scope, element, attrs, controller) {
                var clazz = 'active';
                var path = attrs.href;

                scope.location = location;
                scope.$watch('location.path()', function(newPath) {
                    path = path.replace(window.base, '/');

                    if (path === newPath) {
                        element[0].parentNode.className = 'active';
                    } else {
                        element[0].parentNode.className = '';
                    }
                });
            }
        };
    }]);